import ROOT
#from numpy.random import choice
import numpy as np
import random
import math

# :%s/foo/bar/gc  Change each 'foo' to 'bar', but ask for confirmation first.

# Redefine quit to something shorter
def ex():
  quit()
# Redefine np.sqrt() to sqrt()
def sqrt(x):
  return math.sqrt(x)
def sin(x):
  return math.sin(x)
def cos(x):
  return math.cos(x)
# Redefine pi to something shorter
pi = math.pi
print ''

#///// Set global style /////
ROOT.gROOT.SetStyle('Plain');
ROOT.gStyle.SetPalette(53);
#// Boxes
ROOT.gStyle.SetLegendFont(132);
ROOT.gStyle.SetStatFont(132);
#// Histogram titles
ROOT.gStyle.SetTitleFont(132, 'h');
ROOT.gStyle.SetTitleSize(0.05, 'h'); #0.06
ROOT.gStyle.SetTitleBorderSize(0);
#// Axis titles
ROOT.gStyle.SetTitleFont(132, 'xyz');
ROOT.gStyle.SetTitleSize(0.04, 'xyz'); #0.04
ROOT.gStyle.SetTitleOffset(1.25, 'x'); #1.15
ROOT.gStyle.SetTitleOffset(1, 'y'); #0.7
#// Axis labels
ROOT.gStyle.SetLabelFont(132, 'xyz');
ROOT.gStyle.SetLabelSize(0.04, 'xyz');
#// Text options
ROOT.gStyle.SetTitleFont(132, 't');
ROOT.gStyle.SetTextFont(132);
ROOT.gStyle.SetTextSize(0.025)
ROOT.gStyle.SetPaintTextFormat("3.2g");
#// Legend text size
#ROOT.gStyle.SetLegendTextSize(0.1)
#// Border options
ROOT.gStyle.SetCanvasBorderSize(0);
ROOT.gStyle.SetFrameBorderSize(0);
ROOT.gStyle.SetLegendBorderSize(0);  #1
ROOT.gStyle.SetStatBorderSize(0);
ROOT.gStyle.SetTitleBorderSize(0);
#// Fit options
#ROOT.gStyle.SetOptFit()
ROOT.gROOT.ForceStyle();
print ''


### Markov chain example from https://en.wikipedia.org/wiki/Markov_chain

### Define markets, futures probabilities
markets = ('bull', 'bear', 'stag')

bull = markets[0]
bull2bull = 0.9 # %
bull2bear = 0.075
bull2stag = 0.025
bullFutures = (bull2bull, bull2bear, bull2stag)

bear = markets[1]
bear2bear = 0.8
bear2bull = 0.15
bear2stag = 0.05
bearFutures = (bear2bull, bear2bear, bear2stag)

stag = markets[2]
stag2stag = 0.5
stag2bull = 0.25
stag2bear = 0.25
stagFutures = (stag2bull, stag2bear, stag2stag)

#Market = ( (bull, bull2bull, bull2bear, bull2stag),
#           (bear, bear2bull, bear2bear, bear2stag),
#           (stag, stag2bull, stag2bear, stag2stag) )



stocks = []
stocksN = []

#current = random.choice(markets)

nweeks = 5000
current = bull
currentN = 0.

for week in range(nweeks):
  #stocks.append(current)
  #current = bull

  if current == bull:
    futuresN = +1
    futures = np.random.choice(markets, 1, p=bullFutures)	
  if current == bear:
    futuresN = -1
    futures = np.random.choice(markets, 1, p=bearFutures)           
  if current == stag:
    futuresN = 0
    futures = np.random.choice(markets, 1, p=stagFutures)           

  current = futures[0]
  currentN = currentN+futuresN
  stocks.append(current)
  stocksN.append(currentN)


print ''
print 'After '+str(nweeks)+' weeks:'
print ' '+str(round(stocks.count(bull)/float(len(stocks))*100,2))+'% of market weeks are bull.'
print ' '+str(round(stocks.count(bear)/float(len(stocks))*100,2))+'% of market weeks are bear.'
print ' '+str(round(stocks.count(stag)/float(len(stocks))*100,2))+'% of market weeks are stagnant.'
print ''


stocksW = [float(i) for i in range(len(stocksN))]

c = ROOT.TCanvas('c', 'c', 1366, 720)
g = ROOT.TGraph(len(stocksW), np.array(stocksW), np.array(stocksN))
g.SetTitle('Bull=+1, Stag=0, Bear=-1;Week;Stock value')
g.Draw()

c.SaveAs('MarkovChain_StockMarket.png')

