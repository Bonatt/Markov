### Given transition probabilties over some time period, what percentage of weeks will be in a bull, bear, and stagnant market?

The inspiration for this quick project was an example of a [Markov chains](https://en.wikipedia.org/wiki/Markov_chain) using stock market trends. Here is the a relevant screenshot:

![Markov chain example](WikipediaMarkovChain.png)
 
I mostly just wanted to copy the state diagram. A bull market is a period of generally rising prices, a bear market is a general decline in the stock market over a period of time, and a stagnant market is a period of no change. 
There, linear algebra is employed; here, a _for_ loop is employed.

![Markov chain plot](MarkovChain_StockMarket.png)

```
After 5000 weeks:
 62.6% of market weeks are bull.
 31.22% of market weeks are bear.
 6.18% of market weeks are stagnant.
```

My loop yields the same result as their matrix.
